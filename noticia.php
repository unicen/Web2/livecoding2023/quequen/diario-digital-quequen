<?php 
  require 'db_noticias.php';
  $noticias = getNoticias();
  $id = $_GET['id'];
?>

<?php include 'templates/header.html'; ?>

<main class="container mt-5">
      <section class="noticia">
        <h1><?php echo $noticias[$id]->title ?></h1>
        <img src="<?php echo $noticias[$id]->img ?>"/>
        <p><?php echo $noticias[$id]->text ?></p>
      </section>
    </main>

<?php require 'templates/footer.html'; ?>