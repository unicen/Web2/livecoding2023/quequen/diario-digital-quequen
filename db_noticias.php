<?php
/** 
 * Arhivo PHP puro que define un arreglo de noticias.
 * 
 * Cualquier archivo que lo incluye puede acceder a las noticias definidas.
*/

function getConnection(){
    return new PDO('mysql:host=localhost;'.'dbname=db_diario;charset=utf8', 'root', '');
}

function getNoticias(){
    $db = getConnection();
    $sentencia = $db->prepare("SELECT * FROM news");
    $sentencia->execute();
    $noticias = $sentencia->fetchAll(PDO::FETCH_OBJ);
    return $noticias;
}
?>